import pygame, sprite, tools, pdb
from tools import pg_caption
from pygame.locals import *
from inventory import PlayerInventory
from player_attributes import PlayerAttributes
from rotation import PlayerRotation 
from collections import defaultdict

class Player(PlayerInventory, PlayerAttributes, PlayerRotation):
    def __init__(self, screen, item_list):
        super(Player, self).__init__()
        self.screen = screen
        self.item_list = item_list

        # player coords
        self.x = self.screen.get_size()[0]/2 - 50
        self.y = self.screen.get_size()[1]/2 - 50
        self.actual_coords()

        # player animation
        self.animation = 'idle'
        self.animation_index = 0
        self.animation_speed_init = 7
        self.animation_speed = self.animation_speed_init

        # player attribute delay
        self.delay_energy_init = 60
        self.delay_energy = self.delay_energy_init

        # setting up sprite sheets and maps
        self.sprite_sheet = pygame.image.load('images/player.bmp').convert_alpha()
        self.sprite_map = sprite.map(8, 8, 16, 16)
        self.sprite_dict = {
            'idle' : [4],
            'walk' : [0, 1, 2, 3],
            'you' :  [5]
        }
        sprite.create_global(self.sprite_sheet, self.sprite_map, self.sprite_dict)

        # Player actions
        self.running = False
        self.energy_charge = False

    def draw(self):
        self.sprite = self.rotate()
        self.center_rotation_animation()
        self.screen.blit(self.centered_sprite, (self.center_x, self.center_y))

    def check_for_movement(self):
        """ Checks the movement dictionary to see if motion is present in any direction """
        for direction in self.movement_dict.values():
            if direction[0] == True:
                return True
        return False

    def animate(self):
        """animates the player"""
        self.animation_speed = self.animation_speed - 1
        if self.animation_speed <= 0:
            self.animation_speed = self.animation_speed_init
            self.animation_index = self.animation_index + 1

        self.set_animation()
        self.reset_animation_index()
        self.sprite = sprite.resize(self.animation, self.animation_index)

    def actual_coords(self):
        self.actual_x = self.x + 37
        self.actual_y = self.y + 37
        self.actual_size = 26, 26

    def check_item_interactability(self, item):
        if self.actual_x in item.x_range:
            if self.actual_y in item.y_range:
                return True
        return False

    def run(self):
        if self.running == True:
            self.change_energy(-1)
            self.animation_speed_init = 3
        else:
            self.change_energy(1)
            self.animation_speed_init = 7
            
        if self.energy == 0:
            self.animation_speed_init = 7
            self.running = False
    
    def change_energy(self, num):
        self.delay_energy = self.delay_energy - 1
        if self.delay_energy == 0:
            self.energy = self.energy + num
            self.delay_energy = self.delay_energy_init    
                
    def update(self):
        self.increase_armor()
        self.swap_inventory_items()
        self.angle_range()
        self.actual_coords()
        self.animate()
        self.draw()
        self.run()
        self.attribute_capacity()
        pg_caption(self.energy, self.running, self.energy_charge)
