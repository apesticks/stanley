from consumable_item import ConsumableItem
from armor_item import ArmorItem

def items(screen):
    ham = ConsumableItem(screen, 200, 150, "ham")
    ham.load_image('images/ham.png')

    ham2 = ConsumableItem(screen, 200, 200, "ham")
    ham2.load_image('images/ham.png')

    burger = ConsumableItem(screen, 200, 250, "burger")
    burger.load_image('images/burger.png')

    armor = ArmorItem(screen, 200, 300, "armor")
    armor.load_image('images/armor.png')

    item_list = [burger, ham, ham2, armor]
    return item_list
