from pygame import Surface
import pdb
import pygame

class Sprite(object):
    def __init__(self, filename, unit_size, lines, rows):
        self.filename = filename
        self.unit_size = unit_size
        self.lines = lines
        self.rows = rows

        self.image = pygame.image.load('player.bmp').convert_alpha()
        self.new_size = (400, 400)
        self.surfaces = {}
        """
        self.surfaces = {
            1: {
                1: Surface(),
                2: Surface(),
                ...
            }
            ...
            8: ...
        }
        """

        self.process_image()

    def process_image(self):
        """ This method iterates over each sprite region based on the unit_size.
        For each frame, blit to a new pygame surface and then append that to its 
        unit coordinate in self.surfaces """
        unit_x, unit_y = self.unit_size
        for line_number in range(self.lines):
            line = {}

            # BUGBUG: rows is named incorretly
            for frame_number in range(self.rows):
                # Instantiate a pygame surface
                cropped_surface = Surface((self.unit_size[0], self.unit_size[1]))

                # Blit the current region on to the pygame surface
                # BUGBUG: 0 based indexing of line_number
                cropped_surface.blit(self.image, (0, 0), (line_number * unit_x, line_number * unit_y, unit_x, unit_y))
                cropped_surface = pygame.transform.scale(cropped_surface, self.new_size)

                # Add the current frame surface to its respective frame number in the line dict
                line[frame_number] = cropped_surface

            # Add the current line to its respective line number in the line dict
            self.surfaces[line_number] = line

    def get_surface(self, x, y):
        # Just say no to zero-based indexing
        x -= 1
        y -= 1

        for line_number in self.surfaces.keys():
            line = self.surfaces[line_number]
            for frame_number in line.keys():
                if line_number == x and frame_number == y:
                    return self.surfaces[line_number][frame_number]

        raise Exception("No such frame exists")

    def animate_framelist(self, animation_index, frames):
        for index, frame in enumerate(frames):
            if index == animation_index:
                current_frame = frames[index]
                return self.get_surface(*current_frame)

        raise Exception("Invalid animation index")

if __name__ == "__main__":
    """This is a simple test case. 
    It doesn't actually get imported if you type import ste_sprite"""
    
    pygame.init()
    screen = pygame.display.set_mode((500, 500), 0, 32)
    fps = pygame.time.Clock()

    sprite1 = Sprite("player.bmp", unit_size=(16,16), lines=8, rows=8)

    while True:
        fps.tick(60)

        temp_sprite = sprite1.get_surface(7, 8)
        #temp_sprite = sprite1.animate_framelist(2, [(1, 1), (1, 2), (1, 3)])
        screen.blit(temp_sprite, (0, 0))

        pygame.display.update()
