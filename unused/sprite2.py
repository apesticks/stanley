import pygame

class Sprite(object):

    def __init__(self):
        pass

    def initiate(self, sprite_sheet, sprite_map, sprite_dictionary):
        self.sprite_sheet = sprite_sheet
        self.sprite_map = sprite_map
        self.sprite_dict = sprite_dictionary
        self.animation_speed = None

    def map(self, rows=0, items=0, pixel_x=0, pixel_y=0):
        """Creates a list of passed in items, per passed in number of rows
        seperated by spaces of # of pixels across and down(represents a map of the image"""
        self.pixel_x = pixel_x
        self.pixel_y = pixel_y
        image_map = []
        for row in range(rows):
            for item in range(items):
    	        image_map.append([pixel_x * item, pixel_y * row])
        return image_map

    def resize(self, animation, index, speed, scale_size=(100, 100)):
        """resizes a sprite before blitting or animating it"""
        if self.animation_speed == None:
            self.animation_speed = speed

        self.animation_speed = self.animation_speed - 1
        if self.animation_speed <= 0:
            self.animation_index = index
            self.animation_speed = None

        frame_number = self.sprite_dict[animation][index]
        image = self.sprite_map[frame_number]

        temp_surface = pygame.Surface((self.pixel_x, self.pixel_y))
        temp_surface.set_colorkey((0, 0, 0))
        temp_surface.blit(self.sprite_sheet, (0, 0), (image[0], image[1], self.pixel_x, self.pixel_y))
        sprite = pygame.transform.scale(temp_surface, (scale_size))
        return sprite
