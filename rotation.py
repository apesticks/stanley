import pygame
from collections import defaultdict


class PlayerRotation(object):
    def __init__(self):
        super(PlayerRotation, self).__init__()
        # movement dictionary
        self.movement_dict = {
            'right': [False, 0],
            'up': [False, 90],
            'left': [False, 180],
            'down': [False, 270],
        }

        # initial angle
        self.current_angle = 90
        self.target_angle = 90
        self.direction_queue = []
        self.dimensions = defaultdict(int)
        self.rotation_speed = 12
        self.rotation_increment = 1

    def set_animation(self):
        """sets the animation of the player and gives target_angle"""
        if self.check_for_movement() == True:
            self.animation = 'walk'
            self.target_angle = self.return_target_angle()
        else:
            self.animation = 'idle'

    def return_target_angle(self):
        if self.check_active_directions('up', 'right') == True:
            return 45
        elif self.check_active_directions('up', 'left') == True:
            return 135
        elif self.check_active_directions('down', 'left') == True:
            return 210
        elif self.check_active_directions('down', 'right') == True:
            return 315

        # If no diagonal direction is found, use the direction queue to
        # find the correct angle from the movement_dict
        return self.movement_dict[self.last_queued_angle()][1]

    def angle_range(self):
        """keeps angle in in range of 0 to 360"""
        if self.current_angle > 360: self.current_angle = 0
        if self.current_angle < 0: self.current_angle = 360 - abs(self.current_angle)

    def limit_rotation_increment(self, increment):
        if self.rotation_increment < 0:
            rotation_test = self.current_angle + increment
            if rotation_test > self.target_angle:
                increment = -(self.target_angle - rotation_test)
        return increment

    def quadrant_one_increment(self, increment, shortest_angle_distance):
        if self.target_angle in [270, 315]:
            if self.current_angle in range(0, 90):
                return -increment
        if shortest_angle_distance < 1:
            return -increment
        return increment

    def calc_rotation_increment(self):
        """Returns the the increment that leads to the shortest rotation path"""
        shortest_angle_distance = self.return_shortest_angle_distance()
        increment = self.rotation_speed

        if self.target_angle in [270, 315]:
            if self.current_angle in range(0, 90):
                return -increment
        if shortest_angle_distance < 1:
            return -increment

        increment = self.limit_rotation_increment(increment)

        return increment

    def return_shortest_angle_distance(self):
        shortest_distance = self.target_angle - self.current_angle
        if abs(shortest_distance) > 180:
            shortest_distance = - (shortest_distance - 360)
        return shortest_distance

    def calc_center_rotation(self):
        self.width1, self.height1 = self.sprite.get_size()
        self.centered_sprite = pygame.transform.rotate(self.sprite, self.current_angle)
        self.width2, self.height2 = self.centered_sprite.get_size()

    def center_rotation_animation(self):
        self.center_x = self.x - (self.width2 - self.width1)/2
        self.center_y = self.y - (self.height2 - self.height1)/2

    def reset_animation_index(self):
        animation_length = len(self.sprite_dict[self.animation])
        if self.animation_index >= animation_length:
            self.animation_index = 0

    def last_queued_angle(self):
        """Fetches the most recently queued angle """
        return self.direction_queue[-1]

    def queue_direction(self, direction, dimension):
        self.direction_queue.append(direction)
        self.dimensions[dimension] += 1

    def unqueue_direction(self, direction, dimension):
        if direction in self.direction_queue:
            self.direction_queue.remove(direction)
            self.dimensions[dimension] -= 1

    def check_active_directions(self, *directions):
        """ Iterates over a the supplied arguments to ensure that they
        all reside within the direction list"""
        active_directions = 0
        for direction in directions:
            if direction in self.direction_queue:
                active_directions += 1
                if active_directions == 2:
                    return True
        return False

    def rotate(self):
        """returns a rotation to draw and re-assinges self.sprite"""
        if self.check_for_movement() == True:
            if self.current_angle != self.target_angle:
                self.rotation_increment = self.calc_rotation_increment()
                self.current_angle = self.current_angle + self.rotation_increment

        self.calc_center_rotation()

        return pygame.transform.rotate(self.centered_sprite, self.current_angle)
