import pygame, sys, os, pdb
from pygame.locals import *
from player import Player
from item_objects import items
from map import Map

class Game(object):
    """game processes"""
    def __init__(self, screen_size=(800,600)):
        pygame.init()
        self.fps = pygame.time.Clock()
        self.running = True

        #sets the screen to apear in same position everytime
        self.window_position = 790, 250
        os.environ["SDL_VIDEO_WINDOW_POS"] = str(self.window_position[0]) + "," + str(self.window_position[1])
        self.screen_size = screen_size
        self.screen = pygame.display.set_mode(self.screen_size ,0 , 32)


        #class - method instantiations
        self.item_list = items(self.screen)
        self.player = Player(self.screen, self.item_list)
        self.map = Map(self.screen, self.player, self.item_list)

    def run(self):
        while self.running:
            self.fps.tick(60)
            self.screen.fill((0,0,0))
            self.events()
            self.map.update()
            self.player.update()
            pygame.display.update()

    def events(self):
        for event in pygame.event.get():
            self.game_quit_events(event)
            self.player_movement_events(event)
            self.player_item_interaction_events(event)
            self.inventory_slot_events(event)
            self.testing_events(event)
            self.player_run_events(event)

    def game_quit_events(self, event):
        if event.type == QUIT:
            self.running = False
        if event.type == KEYDOWN:
           if event.key == K_ESCAPE:
               self.running = False

    def player_movement_events(self, event):
        if event.type == KEYDOWN:
            if event.key == K_w:
                self.player.movement_dict['up'][0] = True
                self.player.queue_direction('up', 'vert')
            if event.key == K_s:
                self.player.movement_dict['down'][0] = True
                self.player.queue_direction('down', 'vert')
            if event.key == K_d:
                self.player.movement_dict['right'][0] = True
                self.player.queue_direction('right', 'horiz')
            if event.key == K_a:
                self.player.movement_dict['left'][0] = True
                self.player.queue_direction('left', 'horiz')

        if event.type == KEYUP:
            if event.key == K_w:
                self.player.movement_dict['up'][0] = False
                self.player.unqueue_direction('up', 'vert')
            if event.key == K_s:
                self.player.movement_dict['down'][0] = False
                self.player.unqueue_direction('down', 'vert')
            if event.key == K_d:
                self.player.movement_dict['right'][0] = False
                self.player.unqueue_direction('right', 'horiz')
            if event.key == K_a:
                self.player.movement_dict['left'][0] = False
                self.player.unqueue_direction('left', 'horiz')

    def inventory_slot_events(self, event):
        if event.type == KEYDOWN:
            if event.key == K_1: self.player.set_index(1)
            if event.key == K_2: self.player.set_index(2)
            if event.key == K_3: self.player.set_index(3)
            if event.key == K_4: self.player.set_index(4)
            if event.key == K_5: self.player.set_index(5)
            if event.key == K_6: self.player.set_index(6)
            if event.key == K_7: self.player.set_index(7)
            if event.key == K_8: self.player.set_index(8)
            if event.key == K_9: self.player.set_index(9)
            if event.key == K_0: self.player.set_index(0)

    def player_item_interaction_events(self, event):
        if event.type == KEYDOWN:
            if event.key == K_e:
                self.player.pick_up_item = True
                self.player.add_interactable_item()
            if event.key == K_q:
                self.player.drop_item = True
                self.player.drop_collected_item()
            if event.key == K_f:
                self.player.consume_collected_item()
            if event.key == K_i:
                if self.player.swap_items == False:
                    self.player.swap_items = True
                else:
                    self.player.swap_items = False

    def player_run_events(self, event):
        if event.type == KEYDOWN:
            if event.key == K_LSHIFT:
                if self.player.check_for_movement() == True:
                    self.player.running = True
        if event.type == KEYUP:
            if event.key == K_LSHIFT:
                self.player.running = False

        if self.player.running == True:
            self.map.speed = 4
        else:
            self.map.speed = 2

    def testing_events(self, event):
        if event.type == KEYDOWN:
            if event.key == K_t:
                print "ok"
            if event.key == K_p:
                pdb.set_trace()

