import pygame
from item import Item
from tools import pg_caption

class ConsumableItem(Item):
    """handled in map"""
    def __init__(self, screen, x, y, name):
        super(ConsumableItem, self).__init__(screen, x, y, name)
