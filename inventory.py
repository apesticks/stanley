import pygame
from tools import pg_caption
from custom_inventory_list import InventoryList
from tools import pg_caption

class PlayerInventory(object):
    """handled in player class"""
    def __init__(self):
        super(PlayerInventory, self).__init__()
        self.inventory = InventoryList()
        self.inventory.expand(10)
        self.index = 1
        self.first_swap_index = None
        self.second_swap_index = None
        self.swap_items = False
        self.drop_item = False
        self.pick_up_item = False

    def add_item(self, item):
        self.inventory.add(item)

    def remove_item(self, item=None):
        self.inventory.drop(self.index - 1)
        if self.drop_item == True:
            if item != None:
                item.collected = False

    def set_index(self, index):
        self.index = index

    def add_interactable_item(self):
        for item in self.item_list:
            item.interactable = self.check_item_interactability(item)
            if item.interactable == True:
                if self.pick_up_item == True:
                    if item.collected == False:
                        self.add_item(item)
                        item.collected = True
                        self.pick_up_item = False

    def consume_collected_item(self):
        item = self.return_inventory_index_content()
        if item != None:
            item.consumed = True
            self.remove_item(item)

    def return_inventory_index_content(self):
        if self.inventory[self.index - 1] == []:
            return None
        return self.inventory[self.index - 1][0]

    def drop_collected_item(self):
        item = self.return_inventory_index_content()
        if item != None:
            if item.collected == True:
                if self.drop_item == True:
                    item.x = self.actual_x + item.map_x
                    item.y = self.actual_y + item.map_y
                    self.remove_item(item)
                    self.drop_item = False

    def swap_inventory_items(self):
        if self.swap_items == True:
            first_index, second_index = self.set_swap_values()
            if first_index != None and second_index != None:
                self.inventory.swap(first_index - 1, second_index - 1)
                self.first_swap_index = None
                self.second_swap_index = None
                self.swap_items = False

    def set_swap_values(self):
        if self.first_swap_index == None:
            self.first_swap_index = self.index
        if self.second_swap_index == None:
            self.second_swap_index = self.index
            if self.second_swap_index == self.first_swap_index:
                self.second_swap_index = None

        return self.first_swap_index, self.second_swap_index
