import pygame
global sprite_sheet, sprite_map, sprite_dictionary, pixel_x, pixel_y

def create_global(a, b, c):
    global sprite_sheet, sprite_map, sprite_dictionary
    sprite_sheet = a
    sprite_map = b
    sprite_dictionary = c

def map(rows=0, items=0, px=0, py=0):
    """Creates a list of passed in items, per passed in number of rows
    seperated by spaces of # of pixels across and down(represents a map of the image"""
    global pixel_x, pixel_y
    pixel_x = px
    pixel_y = py
    image_map = []
    for row in range(rows):
        for item in range(items):
	    image_map.append([pixel_x * item, pixel_y * row])
    return image_map

def resize(animation = 'idle', animation_index=0, speed=10, resize_scale=(100, 100)):
    """resizes a sprite before blitting or animating it"""
    frame_number = sprite_dictionary[animation][animation_index]
    image = sprite_map[frame_number]

    temp_surface = pygame.Surface((pixel_x, pixel_y))
    temp_surface.set_colorkey((0, 0, 0))
    temp_surface.blit(sprite_sheet, (0, 0), (image[0], image[1], pixel_x, pixel_y))
    sprite = pygame.transform.scale(temp_surface, (resize_scale))
    return sprite
