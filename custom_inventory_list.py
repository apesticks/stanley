import pdb
class InventoryList(list):
    def __init__(self):
        super(InventoryList, self).__init__()

    def expand(self, size):
        for value in range(size):
            self.append([])

    def swap(self, first_index, second_index):
        try:
            temp = self[first_index]
            self[first_index] = self[second_index]
            self[second_index] = temp
        except IndexError:
            if first_index > len(self) - 1:
                raise IndexError("First index is out of range")
            if second_index > len(self) - 1:
                raise IndexError("Second index is out of range")

    def return_empty_slots(self):
        temp_list = []
        for index, value in enumerate(self):
            if value == []:
                temp_list.append(index)
        return temp_list

    def return_full_slots(self):
        temp_list = []
        for index, value in enumerate(self):
            if value != []:
                temp_list.append((index, value))
        return temp_list

    def check_first_empty_index(self):
        for index, value in enumerate(self):
            if value == []:
                return index

    def check_stackable_item(self, item):
        for index, slot in enumerate(self):
            if slot != []:
                first_value = slot[0]
                if item.name == first_value.name:
                    return index
        return self.check_first_empty_index()

    def add(self, item):
        index = self.check_stackable_item(item)
        self[index].append(item)

    def drop(self, index):
        if self[index] != []:
            self[index].pop(0)

