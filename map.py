import pygame, sys, pdb
from pygame.locals import *
from tools import pg_caption

class Map(object):
    def __init__(self, screen, player, item_list):
        self.screen = screen
        self.player = player
        self.item_list = item_list

        # map attributes
        self.size = 200, 200
        self.color = 0, 100, 0
        self.player_start = self.size[0]/2, self.size[1]/2
        self.screen_size = self.screen.get_size()
        self.x = 0
        self.y = 0

        #map movement and speed
        self.speed = 2
        self.old_speed = self.speed
        self.collided = {
            "up": False,
            "down": False,
            "left": False,
            "right": False,
        }

        #surfaces
        self.surface_data = [
            {"color": (255, 255, 255), "pos": (1, 1), "size": (80,25)},
            {"color": (255, 255, 255), "pos": (20, 50), "size": (15, 25)},
            {"color": (255, 255, 255), "pos": (90, 128), "size": (25, 25)},
        ]
        self.surfaces = []

    def draw_item(self):
        for item in self.item_list:
            item.update(self.x, self.y)

    def draw(self):
        self.surfaces = []
        for surface in self.surface_data:
            color = surface['color']
            pos = surface['pos']
            size = surface['size']

            surface = pygame.draw.rect(self.screen, (color), (pos[0] - self.x, pos[1] - self.y, size[0], size[1]))
            self.surfaces.append(surface)

    def collision_test(self, x, y):
        """ Dynamically generate an x_range and y_range from the supplied
        arguments, return True if any of them fall within a surface x_range
        and y_range"""

        for surface in self.surfaces:
            surface_x_range = range(surface.x, surface.x + surface.size[0])
            surface_y_range = range(surface.y, surface.y + surface.size[1])

            player_x_range = range(x, x + self.player.actual_size[0])
            player_y_range = range(y, y + self.player.actual_size[0])

            for x_val in player_x_range:
                if x_val in surface_x_range:
                    for y_val in player_y_range:
                        if y_val in surface_y_range:
                            return True
        return False

    def check_possible_collision(self):
        return {
            "down": self.collision_test(self.player.actual_x, self.player.actual_y + self.speed),
            "up": self.collision_test(self.player.actual_x, self.player.actual_y - self.speed),
            "left": self.collision_test(self.player.actual_x - self.speed, self.player.actual_y),
            "right": self.collision_test(self.player.actual_x + self.speed, self.player.actual_y),
        }

    def move(self):
        """ Test whether movement in any of the 4 directions would have been a collision"""
        collisions = self.check_possible_collision()

        if self.player.movement_dict['down'][0] == True and collisions['down'] == False:
            self.y = self.y + self.speed

        if self.player.movement_dict['up'][0] == True and collisions['up'] == False:
            self.y = self.y - self.speed

        if self.player.movement_dict['left'][0] == True and collisions['left'] == False:
            self.x = self.x - self.speed

        if self.player.movement_dict['right'][0] == True and collisions['right'] == False:
            self.x = self.x + self.speed

    def update(self):
        self.draw_item()
        self.draw()
        self.move()
