import pygame, pdb
from tools import pg_caption

class Item(object):
    def __init__(self, screen, x, y, name):
        self.screen = screen
        self.x = x
        self.y = y
        #not in use
        self.interactable = False
        self.collected = False
        self.consumed = False
        self.width = self.x
        self.height = self.y
        self.name = name

    def load_image(self, image):
        self.image = pygame.image.load(image)

    def draw(self, x, y):
        """draws the item in relation to the movement of the map"""
        self.calc_map_position(x, y)
        if self.collected == False:
            self.screen.blit(self.image, (self.new_x, self.new_y))

    def calc_map_position(self, x, y):
        self.map_x = x
        self.map_y = y
        self.new_x = self.x - self.map_x
        self.new_y = self.y - self.map_y

    def calc_xy_range(self):
        """checks if a player is in range to interact with the item"""
        self.x_range = range(self.new_x, self.new_x + self.image.get_size()[0])
        self.y_range = range(self.new_y, self.new_y + self.image.get_size()[1])

    def __repr__(self):
        return self.name

    def update(self, x, y):
        self.draw(x, y)
        self.calc_xy_range()


