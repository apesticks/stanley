import pdb
from armor_item import ArmorItem

class PlayerAttributes(object):
    def __init__(self):
        super(PlayerAttributes, self).__init__()
        self.health_points = 100
        self.mana_points = 100
        self.energy = 10
        self.armor = 0

    def increase_armor(self):
        for item in self.item_list:
            if isinstance(item, ArmorItem) == True:
                if item.consumed == True:
                    self.armor = self.armor + item.add_armor()
                    self.item_list.remove(item)
                    assert item not in self.item_list, {"method increase_armor": [item, self.item_list]}
                    
    def attribute_capacity(self):
        if self.health_points > 100: 
            self.health_points = 100
            assert self.health_points <= 100, {"method attribute_capacity": [self.health_points]}
        if self.health_points < 0: 
            self.health_points = 0
            assert self.health_points >= 0, {"method attribute_capacity": [self.health_points]}
        
        if self.mana_points > 100: 
            self.mana_points = 100
            assert self.mana_points <= 100, {"method attribute_capacity": [self.mana_points]}
        if self.mana_points < 0: 
            self.mana_points = 0
            assert self.mana_points >= 0, {"method attribute_capacity": [self.mana_points]}
            
        if self.energy > 10: 
            self.energy = 10
            assert self.energy <= 10, {"method attirbute_capacity": [self.energy]}
        if self.energy < 0: 
            self.energy = 0
            assert self.energy >= 0, {"method attribute_capacity": [self.energy]}